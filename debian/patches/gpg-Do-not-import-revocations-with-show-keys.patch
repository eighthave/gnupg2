From: Werner Koch <wk@gnupg.org>
Date: Tue, 12 Jun 2018 08:44:55 +0200
Subject: gpg: Do not import revocations with --show-keys.

* g10/import.c (import_revoke_cert): Add arg 'options'.  Take care of
IMPORT_DRY_RUN.
--

GnuPG-bug-id: 4017
Signed-off-by: Werner Koch <wk@gnupg.org>
(cherry picked from commit 6c9e37b2e541e96e7c65e1679982a614b4863fdb)
(cherry picked from commit e8f439e0547463c24f3c10008fee73e6c4259f52)
---
 g10/import.c | 56 ++++++++++++++++++++++++++++++--------------------------
 1 file changed, 30 insertions(+), 26 deletions(-)

diff --git a/g10/import.c b/g10/import.c
index b609660..ed3ada5 100644
--- a/g10/import.c
+++ b/g10/import.c
@@ -113,8 +113,8 @@ static int import_secret_one (ctrl_t ctrl, kbnode_t keyblock,
                               struct import_stats_s *stats, int batch,
                               unsigned int options, int for_migration,
                               import_screener_t screener, void *screener_arg);
-static int import_revoke_cert (ctrl_t ctrl,
-                               kbnode_t node, struct import_stats_s *stats);
+static int import_revoke_cert (ctrl_t ctrl, kbnode_t node, unsigned int options,
+                               struct import_stats_s *stats);
 static int chk_self_sigs (ctrl_t ctrl, kbnode_t keyblock, u32 *keyid,
                           int *non_self);
 static int delete_inv_parts (ctrl_t ctrl, kbnode_t keyblock,
@@ -590,7 +590,7 @@ import (ctrl_t ctrl, IOBUF inp, const char* fname,struct import_stats_s *stats,
                                 screener, screener_arg);
       else if (keyblock->pkt->pkttype == PKT_SIGNATURE
                && IS_KEY_REV (keyblock->pkt->pkt.signature) )
-        rc = import_revoke_cert (ctrl, keyblock, stats);
+        rc = import_revoke_cert (ctrl, keyblock, options, stats);
       else
         {
           log_info (_("skipping block of type %d\n"), keyblock->pkt->pkttype);
@@ -2617,7 +2617,8 @@ import_secret_one (ctrl_t ctrl, kbnode_t keyblock,
  * Import a revocation certificate; this is a single signature packet.
  */
 static int
-import_revoke_cert (ctrl_t ctrl, kbnode_t node, struct import_stats_s *stats)
+import_revoke_cert (ctrl_t ctrl, kbnode_t node, unsigned int options,
+                    struct import_stats_s *stats)
 {
   PKT_public_key *pk = NULL;
   kbnode_t onode;
@@ -2707,31 +2708,34 @@ import_revoke_cert (ctrl_t ctrl, kbnode_t node, struct import_stats_s *stats)
   /* insert it */
   insert_kbnode( keyblock, clone_kbnode(node), 0 );
 
-  /* and write the keyblock back */
-  rc = keydb_update_keyblock (ctrl, hd, keyblock );
-  if (rc)
-    log_error (_("error writing keyring '%s': %s\n"),
-               keydb_get_resource_name (hd), gpg_strerror (rc) );
-  keydb_release (hd);
-  hd = NULL;
-
-  /* we are ready */
-  if (!opt.quiet )
+  /* and write the keyblock back unless in dry run mode.  */
+  if (!(opt.dry_run || (options & IMPORT_DRY_RUN)))
     {
-      char *p=get_user_id_native (ctrl, keyid);
-      log_info( _("key %s: \"%s\" revocation certificate imported\n"),
-                keystr(keyid),p);
-      xfree(p);
-    }
-  stats->n_revoc++;
+      rc = keydb_update_keyblock (ctrl, hd, keyblock );
+      if (rc)
+        log_error (_("error writing keyring '%s': %s\n"),
+                   keydb_get_resource_name (hd), gpg_strerror (rc) );
+      keydb_release (hd);
+      hd = NULL;
 
-  /* If the key we just revoked was ultimately trusted, remove its
-     ultimate trust.  This doesn't stop the user from putting the
-     ultimate trust back, but is a reasonable solution for now. */
-  if (get_ownertrust (ctrl, pk) == TRUST_ULTIMATE)
-    clear_ownertrusts (ctrl, pk);
+      /* we are ready */
+      if (!opt.quiet )
+        {
+          char *p=get_user_id_native (ctrl, keyid);
+          log_info( _("key %s: \"%s\" revocation certificate imported\n"),
+                    keystr(keyid),p);
+          xfree(p);
+        }
 
-  revalidation_mark (ctrl);
+      /* If the key we just revoked was ultimately trusted, remove its
+       * ultimate trust.  This doesn't stop the user from putting the
+       * ultimate trust back, but is a reasonable solution for now. */
+      if (get_ownertrust (ctrl, pk) == TRUST_ULTIMATE)
+        clear_ownertrusts (ctrl, pk);
+
+      revalidation_mark (ctrl);
+    }
+  stats->n_revoc++;
 
  leave:
   keydb_release (hd);
